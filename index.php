<?php 

require('animal.php');
$sheep = new  Animal("shaun");
echo "Name : " .$sheep->name ."<br>"; // "shaun"
echo "legs : " .$sheep->legs ."<br>"; // 4
echo "Cold Blooded : " .$sheep->cold_blooded ."<br><br>"; 

require('frog.php');
$kodok = new  Frog("Buduk");
echo "Name : " .$kodok->name ."<br>"; 
echo "legs : " .$kodok->legs ."<br>"; 
echo "Cold Blooded : " .$kodok->cold_blooded ."<br>"; 
echo "Jump : " .$kodok->jump ."<br><br>";

require('ape.php');
$sungokong = new  Ape("kera sakti");
echo "Name : " .$sungokong->name ."<br>"; 
echo "legs : " .$sungokong->legs ."<br>";
echo "Cold Blooded : " .$sungokong->cold_blooded ."<br>"; 
echo "Yel : " .$sungokong->yel ."<br><br>";
?>